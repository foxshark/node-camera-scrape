#Tier 2
SELECT i2.key1, k2.keyword as key2, i2.title, i2.id
FROM keywords as k2
INNER JOIN (
	SELECT k1.keyword as key1, i.title, i.id
	FROM keywords as k1
	INNER JOIN (
		SELECT id, title
		FROM feedposts
		WHERE title LIKE "%1.8g%"
		ORDER BY ID DESC
		LIMIT 10
	) as i
	ON i.title LIKE CONCAT("%",k1.keyword,"%")
	WHERE k1.tier = 1
	GROUP BY i.id
) as i2
ON i2.title LIKE CONCAT("%",k2.keyword,"%")
WHERE k2.tier = 2
GROUP BY i2.id
;

#Tier 3
SELECT i3.key1, i3.key2, k3.keyword as key3, i3.title, i3.id
FROM keywords as k3
INNER JOIN (
	SELECT i2.key1, k2.keyword as key2, i2.title, i2.id
	FROM keywords as k2
	INNER JOIN (
		SELECT k1.keyword as key1, i.title, i.id
		FROM keywords as k1
		INNER JOIN (
			SELECT id, title
			FROM feedposts
			WHERE title LIKE "%1.8g%"
			ORDER BY ID DESC
			LIMIT 10
		) as i
		ON i.title LIKE CONCAT("%",k1.keyword,"%")
		WHERE k1.tier = 1
		GROUP BY i.id
	) as i2
	ON i2.title LIKE CONCAT("%",k2.keyword,"%")
	WHERE k2.tier = 2
	GROUP BY i2.id
) as i3
ON i3.title LIKE CONCAT("%",k3.keyword,"%")
WHERE k3.tier = 3
GROUP BY i3.id
;

#test tier from item
SELECT *
FROM keywords
WHERE id IN (
SELECT
CASE
	WHEN synonym IS NOT NULL THEN synonym
-- 	WHEN parent IS NOT NULL THEN parent
	ELSE id
END as top_number
FROM keywords
WHERE keyword IN ("Nikkor","35mm","f/1.8G","AF-S","DX","Lens","for","Nikon","Digital","SLR","Cameras","-","Excellent","Cond")#,"and","50mm","1.4","ai-s")
)
ORDER BY tier ASC, keyword ASC
;

#item naming test
SELECT items.id, items.name, GROUP_CONCAT(keywords.keyword ORDER BY keywords.tier ASC, keywords.keyword ASC SEPARATOR " ")
FROM items, keywords, items_keywords
WHERE items.id = items_keywords.item_id
AND keywords.id = items_keywords.keyword_id
GROUP BY items.id
ORDER BY keywords.tier ASC, keywords.keyword ASC
;
