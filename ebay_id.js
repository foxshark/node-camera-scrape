var md5 = require('md5');
const console = require('better-console');

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'camerascrape'
});
var lastId = 0;
 
connection.connect();
getFeedpostBlock();

function getFeedpostBlock()
{
	connection.query(
		`SELECT *
		FROM feedposts_historical
		WHERE id>?
		ORDER BY id ASC
		LIMIT 10000`,
		[lastId],
		function (error, results, fields) {
			console.log("Last ID: "+lastId);
			results.forEach(function(entry) {
				lastId = Math.max(lastId,entry.id);
				storeFreshItem(entry);
			});
			if(results.length>0) {
				getFeedpostBlock();
			}
		});
}

function storeFreshItem(entry) {
	var params = [
		entry.id,
		entry.link.replace(/.*\/|\?.*/gi, ''),
		entry.content,
		entry.contentSnippet,
		entry.isoDate,
		entry.isodatetime,
		entry.link,
		entry.pubdate,
		entry.pubdatetime,
		entry.title,
		entry.price,
		entry.image,
		entry.feed_id
	];

	connection.query('INSERT IGNORE INTO feedposts (id, ebay_id, content, content_snippet, isodate, isodatetime, link, pubdate, pubdatetime, title, price, image, feed_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', params, function (error, results, fields) {
		//connection.query('INSERT INTO feedposts (content) VALUES (?) ;', params, function (error, results, fields) {
			  if (error) throw error;
			});
}