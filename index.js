var express = require('express');
 
var app = express();
var router = express.Router();
var path = __dirname + '/views/';
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'camerascrape'
});
connection.connect();


app.set('views', './views');
app.set('view engine', 'jade');


app.get('/', function(req, res) {
	res.render('grid', {
		title: 'Camera Scrape'
	});  
});

app.get('/high', function(req, res) {
	res.render('sold', {
		title: 'Camera Scrape'
	});  
});


app.get('/short', function(req, res) {
	res.render('soldshort', {
		title: 'Camera Scrape'
	});  
});

app.get('/feed/:feedid', function(req, res) {
	var feedId = req.params.feedId;
	console.log(req.params);
	res.render('grid', {
		title: 'Camera Scrape'
	});
});

app.get('/items/', function(req, res) {
	connection.query(`
		SELECT items.id, items.brand, items.name as name, items.avg_price as avg_price #, feedposts.*
		FROM items, feedposts_items, feedposts
		WHERE items.id = feedposts_items.item_id
		AND feedposts.id = feedposts_items.feedpost_id
		AND feedposts.processed = 1
		GROUP BY items.id
		ORDER BY items.name DESC`
		, function (error, results, fields) {
			if (error)  throw error;
			// res.setHeader('Content-Type', 'application/json');
			// res.send(JSON.stringify(results));
			res.render('itemlist', {
				title: 'Item List',
				items: results
			});  
		});			
	
});

app.get('/items/:itemId', function(req, res) {
	var itemId = req.params.itemId;
	var params = [itemId];
	connection.query(`
		SELECT feedposts.*, 
			FORMAT(feedposts.price, 2) AS price_dollars, 
			DATE_FORMAT(FROM_UNIXTIME(pubdatetime), "%m/%d/%y") as date_pub,
			DATE_FORMAT(FROM_UNIXTIME(soldtime), "%m/%d/%y") as date_sold,
			IF(soldtime > 0, (CEIL((soldtime - pubdatetime)/(60*60*24))), NULL) as days_on_market,
			# items.avg_price as avg_price
			FORMAT((feedposts.price / items.avg_price * 100),1) as price_diff
		FROM feedposts_items, feedposts, items
		WHERE feedposts.id = feedposts_items.feedpost_id
		AND items.id = feedposts_items.item_id
		AND feedposts_items.item_id = ?
		AND feedposts.processed = 1
		ORDER BY feedposts.id DESC`
		, params, function (error, results, fields) {
			if (error)  throw error;
			// res.setHeader('Content-Type', 'application/json');
			res.render('item', {
				title: results.length+' results for item #'+itemId,
				items: results
			});  
		});
});

app.get('/deals/', function(req, res) {
	connection.query(`
		SELECT items.name, 
		# 	items.avg_price, 
			(100-(FLOOR(feedposts.price/items.avg_price*100))) as off, 
			feedposts.title,
			FORMAT(feedposts.price, 2) AS price_dollars, 
			DATE_FORMAT(FROM_UNIXTIME(pubdatetime), "%m/%d/%y") as date_pub,
			feedposts.guid,
			feedposts.soldtime
		FROM feedposts, feedposts_items, items
		WHERE feedposts_items.item_id = items.id
		AND feedposts_items.feedpost_id = feedposts.id
		AND feedposts.processed = 1
		AND (UNIX_TIMESTAMP() - feedposts.pubdatetime) < (60*60*24*7) # less than a week old
		ORDER BY off DESC
		LIMIT 100;`
		, function (error, results, fields) {
			if (error)  throw error;
			// res.setHeader('Content-Type', 'application/json');
			// res.send(JSON.stringify(results));
			res.render('deallist', {
				title: 'Deal List',
				items: results
			});  
		});			
	
});


app.get('/feedid', function(req, res) {
	var mysql      = require('mysql');
	var connection = mysql.createConnection({
	  host     : '127.0.0.1',
	  user     : 'root',
	  password : '',
	  database : 'camerascrape'
	});
	 
	connection.connect();
	connection.query('SELECT * FROM feedposts WHERE feed_id = ? ORDER BY id DESC LIMIT 100', function (error, results, fields) {
		connection.end();
		if (error)  throw error;
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(results));
	});			
});

app.get('/grid', function(req, res) {
	var mysql      = require('mysql');
	var connection = mysql.createConnection({
	  host     : '127.0.0.1',
	  user     : 'root',
	  password : '',
	  database : 'camerascrape'
	});
	 
	connection.connect();
	connection.query('SELECT * FROM feedposts ORDER BY id DESC LIMIT 50', function (error, results, fields) {
		connection.end();
		if (error)  throw error;
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(results));
	});			
});

app.get('/sold', function(req, res) {
	var mysql      = require('mysql');
	var connection = mysql.createConnection({
	  host     : '127.0.0.1',
	  user     : 'root',
	  password : '',
	  database : 'camerascrape'
	});
	 
	connection.connect();
	connection.query('SELECT * FROM feedposts WHERE soldtime IS NOT NULL ORDER BY price DESC LIMIT 100', function (error, results, fields) {
		connection.end();
		if (error)  throw error;
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(results));
	});			
});

app.get('/soldshort', function(req, res) {
	var mysql      = require('mysql');
	var connection = mysql.createConnection({
	  host     : '127.0.0.1',
	  user     : 'root',
	  password : '',
	  database : 'camerascrape'
	});
	 
	connection.connect();
	connection.query('SELECT *, (soldtime-pubdatetime) as timeonmarket FROM feedposts WHERE soldtime IS NOT NULL AND pubdate > timestamp("2019-01-01") ORDER BY timeonmarket ASC LIMIT 100', function (error, results, fields) {
		connection.end();
		if (error)  throw error;
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(results));
	});			
});

app.use(express.static(__dirname + '/public'));
 
app.listen(8087,function(){
  console.log("Live at Port 8087");
});