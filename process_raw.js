// basic training proof of concept: scanners
const console = require('better-console');
const readline = require('readline');
const mysql      = require('mysql');
const md5 = require('md5');
const bayes = require('bayes');
const slugify = require('slugify')

const connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'camerascrape'
});

// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// });

var debugMode = false;
var qOffset = 10000;

// t1Classifier = bayes();
 

var itemExampleIds = [3330, 2954, 2955, 2956, 2957, 2958, 2959, 2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 2968, 2969, 2970, 2971, 2972, 2973, 2974, 2975, 2976, 2977, 2978, 2979, 2980, 2981, 2982, 2983, 2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991, 2992, 2993, 2994, 2995, 2996, 2997, 2998, 2999, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019, 3020, 3021, 3022, 3023, 3024, 3025, 3026, 3027, 3028, 3029, 3030, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3039, 3040, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3048, 3049, 3050, 3051, 3052, 3053, 3054, 3055, 3056, 3057, 3058, 3059, 3060, 3061, 3062, 3063, 3064, 3065, 3066, 3067, 3068, 3069, 3070, 3071, 3072, 3073, 3074, 3075, 3076, 3077, 3078, 3079, 3080, 3081, 3082, 3083, 3084, 3085, 3086, 3087, 3088, 3089, 3090, 3091, 3092, 3093, 3094, 3095, 3096, 3097, 3098, 3099, 3100, 3101, 3102, 3103, 3104, 3105, 3106, 3107, 3108, 3109, 3110, 3111, 3112, 3113, 3114, 3115, 3116, 3117, 3118, 3119, 3120, 3121, 3122, 3123, 3124, 3125, 3126, 3127, 3128, 3129, 3130, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149, 3150, 3151, 3152, 3153, 3154, 3155, 3156, 3157, 3158, 3159, 3160, 3161, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169, 3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179, 3180, 3181, 3182, 3183, 3184, 3185, 3186, 3187, 3188, 3189, 3190, 3191, 3192, 3193, 3194, 3195, 3196, 3197, 3198, 3199, 3200, 3201, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209, 3210, 3211, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219, 3220, 3221, 3222, 3223, 3224, 3225, 3226, 3227, 3228, 3229, 3230, 3231, 3232, 3233, 3234, 3235, 3236, 3237, 3238, 3239, 3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248, 3249, 3250, 3251, 3252, 3253, 3254, 3255, 3256, 3257, 3258, 3259, 3260, 3261, 3262, 3263, 3264, 3265, 3266, 3267, 3268, 3269, 3270, 3271, 3272, 3273, 3274, 3275, 3276, 3277, 3278, 3279, 3280, 3281, 3282, 3283, 3284, 3285, 3286, 3287, 3288, 3289, 3290, 3291, 3292, 3293, 3294, 3295, 3296, 3297, 3298, 3299, 3300, 3301, 3302, 3303, 3304, 3305, 3306, 3307, 3308, 3309, 3310, 3311, 3312, 3313, 3314, 3315, 3316, 3317, 3318, 3319, 3320, 3321, 3322, 3323, 3324, 3325, 3326, 3327, 3328, 3329];


itemExampleIds.forEach(function(example) {
	// trainKnownItem(example);
});


var goodItems = [];
var badItems = [];

var manualIds = process.argv.slice(2);
if(manualIds.length > 0) {
	debugMode = true;
	getFeedpostsToExplain(manualIds);
} else {
	//default behavior
	getItemsToClassify();
}



function sanatizeString(string, strict = true)
{
	if(strict) {
		string = string.toLowerCase().split(" for ",1).shift();
		string = string.toLowerCase().split(" and ",1).shift();
	}
	string = string.toLowerCase().split(" with ",1).shift();
	string = string.toLowerCase().split(" w/ ",1).shift();
	string = string.replace('\t','').trim();
	string = string.replace('(','');
	string = string.replace(')','');
	return string;
}

function sanatizeLensString(string)
{
	string = string.replace(/\d[dge]/gi, function(s) { //fixes f/2.0D f/2.8E f/5.6G
		return ( s.substring(0,s.length-1) + " " + s.substring(s.length-1, s.length) );
	});
	string = string.replace(/\d\s+m{2}|\d-\s+\d|\d\s+-\d/gi, function(s){ // fixes: 24 mm, 12 -24mm, 12- 24mm
		return s.replace(' ', '');
	});
	string = string.replace(/[f\/-]\d+\.0\s/g, function(s){ // fixes: f/2.8-4.0, f/4.0 > f/4
		return ( s.substring(0,s.length-3) + " ");
	})
	string = string.replace(/\d+-\d+\s/g, function(s){ // fixes 80-200 > 80-200mm
		return ( s.substring(0,s.length-1) + "mm ");
	})
	//misc fixes
	string = string.replace("series e", "series_e");
	string = string.replace("non ai", "non_ai");
	string = string.replace("pre ai", "pre_ai");
	if(! string.includes("f/")) {
		string = string.replace(/\s\d+\.\d+\s/, function(s){ // fixes 80-200 > 80-200mm
			return ( " f/"+s.substring(1));
		})	
	}
	return string;
}

// function splitTrain(words, category)
// {
// 	words.split(' ').forEach(function(word) {
// 		t1Classifier.learn(word, category);
// 	});
// }

function splitItemIntoKeywords(item, callback)
{
	searchKeywords(item, function(result) {
		console.log(item);
		console.log(result);
	});
}

function searchKeywords(itemName, callback)
{
	itemName = itemName.replace(/\s\s+/g, ' '); // reduce multiple white space in string
	itemName = sanatizeLensString(sanatizeString(itemName, false)); // remove tabs, trim
	// console.warn("item sanitized name: "+itemName);
	var keywordList = itemName.split(' '); 
	var params = [keywordList];
	var slugParts = [];
	var keywordId = [];
	var highestTier = 0;
	if(debugMode){
		console.warn("Item Keywords: ");
		console.warn(keywordList);
	} 

	return connection.query(`
	SELECT id, keyword, tier
	FROM keywords
	WHERE id IN (
	SELECT
	CASE
		WHEN synonym IS NOT NULL THEN synonym
		ELSE id
	END as top_number
	FROM keywords
	WHERE keyword IN (?)
	)
	AND tier < 8 # cut out relevant but fluff keywords
	ORDER BY tier ASC, keyword ASC
	;`, params, function (error, results, fields) {
		if (error) throw error;
		results.forEach(function(entry) { 
			slugParts.push(entry.keyword);
			keywordId.push(entry.id)
			if(highestTier+1 == entry.tier) highestTier = entry.tier;
		});	
		return callback({
			highestTier: highestTier,
			slugParts: slugParts,
			keywordId: keywordId,
			slug: slugify(slugParts.join(" "), {remove: /[-/*+~.()'"!:@]/g,lower:true})
		});
	});
}

function classifyItem(item, callback)
{
	var itemName = item.title;
	searchKeywords(itemName, function(keyResult) {
		// console.log(itemName);
		//  console.log(keyResult);
		// var params = [keyResult.keywordId.length]; //keyResult.keywordId,
		if(!keyResult.keywordId.length){
			console.warn("Classify Failed with no keywords: #" + item.id + " " + itemName);
			return callback([]);
		} else {
			compareToItems(keyResult.keywordId, function(itemSet) {
				if(!itemSet.length) {
					if(debugMode){
						console.warn(keyResult);
					}
					salvageClassification(item, keyResult, function(newItemSet) {
						if(!newItemSet.length) {
							console.error("Classify failed with keywords, but no items with keyword matches: #" + item.id + " " + itemName);
						} else {
							console.log("Classify salvaged for: "+itemName);
						}
						return callback(newItemSet);
					});
				} else {
					console.log("Classify found "+itemSet.length+" matches for "+itemName);
					if(itemSet.length > 1) {
						console.warn(keyResult);
					}
					return callback(itemSet); 
				}
			});
		}
	});
}

function salvageClassification(item, failedQueryResult, callback)
{
	// get keywords of tier level-1
	var reducedTier = failedQueryResult.highestTier-1;
	var params = [failedQueryResult.keywordId, reducedTier];
	connection.query(`
		SELECT id, keyword, tier
		FROM keywords
		WHERE id IN (?)
		AND tier < ?
		ORDER BY tier ASC, keyword ASC
		;`, params, function (error, results, fields) {
			if (error) throw error;
			var reducedItemNameParts = [];
			var reducedItemKeywordIds = [];
			results.forEach(function(entry) { 
				reducedItemKeywordIds.push(entry.id);
				reducedItemNameParts.push(entry.keyword);
			});	
			var reducedItemName = reducedItemNameParts.join(" ");
			if(reducedItemKeywordIds.length > 0) {
				compareToItems(reducedItemKeywordIds, function(itemSet) {
					if(debugMode && itemSet.length > 0) {
						getItemsToExplain(itemSet);
					}
					if(itemSet.length > 0) {
						getItemKeywords(itemSet, function(realItemKeywordIdSet){
							var badItemKeywordIdSet = failedQueryResult.keywordId;
							var correctedItemKeywordIdSet = badItemKeywordIdSet.filter(value => -1 !== realItemKeywordIdSet.indexOf(value));
							if(debugMode) {
								console.log(realItemKeywordIdSet);
								console.warn(badItemKeywordIdSet);
								console.warn(correctedItemKeywordIdSet);
							}

							//now mach against that
							compareToItems(correctedItemKeywordIdSet, function(itemSet) {
								if(!itemSet.length) {
									console.error("Classify failed with keywords, but no matches: # " + item.id + " " + item.title);
									if(debugMode){
										console.warn(itemSet);
									}
								} else {
									if(itemSet.length > 1) {
										console.warn("Classify returned multiple matches for #" + item.id);
									} 
								}
								if(debugMode && itemSet.length>0	) {
									console.warn("Fixing bad title: "+item.title+" to match:");
									getItemsToExplain(itemSet);
								}
								return callback(itemSet);
							});
						});
					} else {
						return callback([]);
					}
				});
			} else {
				return callback([]);
			}

			console.warn("Reduced #" + item.id + ": " + reducedItemName);
			// next step: get all matched of above, eliminate sub-keywords agasint this one's


			// return callback({
			// 	highestTier: highestTier,
			// 	slugParts: slugParts,
			// 	keywordId: keywordId,
			// 	slug: slugify(slugParts.join(" "), {remove: /[-/*+~.()'"!:@]/g,lower:true})
			// });
		});
}

function getItemsToClassify(callback)
{
	connection.query(`
	SELECT id, title
	FROM feedposts
	WHERE feed_id = 2
	AND (title LIKE "%nikon%"
	OR title LIKE "%canon%")
	AND (
			title NOT LIKE "%box only%"
		OR 	title NOT LIKE "%empty box%"
	)
	AND processed IS NULL
	ORDER BY id DESC
	LIMIT 20000
	;`, function (error, results, fields) {
		if (error) throw error;
		console.log("Found "+results.length+" feedposts to work");
		results.forEach(function(item){
			classifyItem(item, function(itemMatches) {
				if(itemMatches.length == 1) {
					console.log("Write: "+item.title+" successful with 1 match");
					writeFeedpostItemMatch(item.id, itemMatches[0]);
					
				}
				markItemProcessed(item.id, itemMatches.length);
			});
			// process.exit();
			// if(typeof itemMatches != undefined && itemMatches.length == 1) {
			// 	writeFeedpostItemMatch(item.id, itemMatches[0].id);
			// 	console.log("wrote " + item.name);
			// }
		});
		writeAvgPrice();
		// return callback(true);

	});
}

function getFeedpostsToExplain(feedposts) 
{
	var params = [feedposts];
	connection.query(`
		SELECT id, title
		FROM feedposts
		WHERE id IN (?)
		;`, params, function (error, results, fields) {
		if (error) throw error;
		console.log("Found "+results.length+" feedposts to work");
		results.forEach(function(item){
			classifyItem(item, function(itemMatches) {
				console.log(item.id+" : "+item.title);
				console.warn(itemMatches);
				if(itemMatches.length>0) {
					getItemsToExplain(itemMatches);
				}
			});
		});
	});
}

function getItemsToExplain(items)
{
	var params = [items];
	connection.query(`
		SELECT id, name
		FROM items
		WHERE id IN (?)
		;`, params, function (error, results, fields) {
		if (error) throw error;
		var itemNames = [];
		results.forEach(function(item){
			itemNames.push(item.name);
		});

		console.log("Found "+results.length+" items to match for salvage");
		console.log(items);
		console.log(itemNames);
	});
}

function getItemKeywords(items, callback)
{
	//get all the keywords from a set of items
	var params = [items];
	connection.query(`
		SELECT DISTINCT keyword_id
		FROM items_keywords
		WHERE item_id IN (?)
		;`, params, function (error, results, fields) {
		if (error) throw error;
		var keywordIds = [];
		results.forEach(function(row){
			keywordIds.push(row.keyword_id);
		});
		return callback(keywordIds);
	});
}

function markItemProcessed(itemId, statusId)
{
	// null/0 = unprocessed
	// >1 = number of matches
	// 1 = pass
	var params = [statusId, itemId];

	connection.query(`
		UPDATE feedposts
		SET processed=?
		WHERE id=?
		`, params, function (error, results, fields) {
		if (error) throw error;
	});	
}

function writeFeedpostItemMatch(feedpostId, itemId)
{
	var params = [feedpostId, itemId, 1]; //keyResult.keywordId,
	connection.query(`
	INSERT IGNORE INTO feedposts_items (feedpost_id, item_id, visible)
	VALUES (?, ?, ?)
	;`, params, function (error, results, fields) {
		if (error) throw error;
	});	
}

function compareToItems(idSet, callback)
{
	var params = [idSet, idSet.length]; //keyResult.keywordId,
	return connection.query(`
	SELECT items.*
	FROM items, items_keywords
	WHERE items.id = items_keywords.item_id
	AND items_keywords.keyword_id IN (?)
	GROUP BY items.id
	HAVING count(*) = ?
	;`, params, function (error, results, fields) {
		if (error) throw error;
		foundItemIds = [];
		results.forEach(function(item){
			// console.log("Search found #"+item.id+" "+item.name);
			foundItemIds.push(item.id); //+" "+item.name);
		});
		
		return callback(foundItemIds);
	});	
}


function writeAvgPrice()
{
	console.log();
	return connection.query(`
	UPDATE items
	SET avg_price = (
		SELECT  AVG(feedposts.price)
		FROM feedposts_items, feedposts
		WHERE feedposts.id = feedposts_items.feedpost_id
		AND feedposts_items.item_id = items.id
		AND feedposts.processed = 1
		)
	;`, function(error, results, fields) {
		if (error) throw error;
		console.log("Writing avg values for items.");
	});	
}

// function trainKnownItem(itemId)
// {
// 	//EX: Nikon AF-S Nikkor 300mm f/2.8G VR IF-ED	
// 	var params = [itemId];
// 	connection.query(`
// 	SELECT id, concat(brand, " ",name) as item_name
// 	FROM items
// 	WHERE id = ?
// 	;`, params, function (error, results, fields) {
// 		if (error) throw error;
// 		results.forEach(function(entry) { 
// 			searchKeywords(entry.item_name, function(keyResult) {
// 				if(keyResult.highestTier >= 3) {
// 					console.log("writing T"+ keyResult.highestTier+" item " + entry.item_name);
// 					writeItemKeywords(itemId, keyResult.keywordId);
// 				} else {
// 					console.warn("Item max tier is insufficient: T" + keyResult.highestTier + " for " + entry.item_name);
// 					console.error(keyResult);
// 					// process.exit();
// 				}
// 			});
// 		});	
// 	});
// }

function writeItemKeywords(itemId, keywordIds)
{
	// console.log(Array.isArray(keywordIds));
	// process.exit();
	// console.log("writing item "+ itemId);
	var pairs = [];
	keywordIds.forEach(function(keywordId) {
		pairs.push([itemId, keywordId]);
	});
	var params = [pairs];
	connection.query(`
	DELETE FROM
	items_keywords
	WHERE item_id = ?
	;`, itemId, function (error, results, fields) {
		if (error) throw error;
		connection.query(`
		INSERT INTO items_keywords
		(item_id, keyword_id)
		VALUES ?
		;`, params, function (error, results, fields) {
			if (error) throw error;
		});
	});
}

/*
// Semi-guided, cascading classification tree
Training
1. load tier 1 examples per tier item
2. confirm classification until P is sufficiently high
3. load next tier..

Usage
1. classify record on tier cascade
2. drop n-1 tiers(s) when P is insufficient or tiers have been exhausted

Ex results
1. nikon-50mm
2. nikon-50mm-f1.8-afd : also positive for users searching for #1
3. nikon-50mm-f1.8-g : also positive for #1, negative for #2
*/



// useb by oher things!!



exports.processItemSet = function()
{
	fixURL();
	console.log("start raw processing");
	connection.query('SELECT feedposts_raw.content FROM feedposts_raw LEFT JOIN solditems ON feedposts_raw.item_hash = solditems.item_hash WHERE solditems.item_hash IS NULL LIMIT 1000', function (error, res, fields) {
		if (error)  throw error;
		//console.table(res);
		//console.log(JSON.stringify(res));
		if(res) {
			console.log(res.length + " raw records found.")
			res.forEach(function(itemData) {
				//console.table(JSON.parse(item.content));
				var item = JSON.parse(itemData.content);
				try {
					recordTransaction(item);
				} catch(err) {
					console.log(item.tile + " was malformatted");
				}
			});
		} else {
			console.log("could not connect to DB");
			// process.exit;
		}
	});
}

function recordTransaction(item)
{
	var itemId = item.link.split("?").shift().split("/").pop();
	var startTime = Date.parse(item.isoDate)/1000;
	var endTime = (item.EndTime._/1000);

	var params = [
			md5(item.guid),
			itemId,
		 	item.title,
		 	(item.CurrentPrice._/100),
		 	(endTime - startTime),
		 	endTime,
		 	item.guid,
	 	];

	connection.query('INSERT IGNORE INTO solditems (item_hash, item_id, title, cost, time_on_market, sold_date, item_url) VALUES (?,?,?,?,?,?,?);',
		params, 
		function (error, res, fields) {
			if (error)  throw error;
			//console.log(item.title);
		});
}

function fixURL()
{
	connection.query('SELECT feedposts_raw.content, solditems.id FROM feedposts_raw, solditems WHERE feedposts_raw.item_hash = solditems.item_hash AND solditems.item_url IS NULL LIMIT 5000', function (error, res, fields) {
		if (error)  throw error;
		//console.table(res);
		//console.log(JSON.stringify(res));
		if(res && res.length > 0) {
			console.log(res.length + " missing URL records found.")
			res.forEach(function(itemData) {
				var item = JSON.parse(itemData.content);
				var params = [item.guid, itemData.id];
				//console.log(params);
				connection.query('UPDATE solditems SET item_url = ? WHERE id = ?', params, function (error2, res, fields) {
					if (error2)  {
						throw error2;
					}
				});	
			});
		}
	});				
}
