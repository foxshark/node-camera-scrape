// basic training proof of concept: scanners
const console = require('better-console');
const readline = require('readline');
const mysql      = require('mysql');
const md5 = require('md5');
const bayes = require('bayes');
const slugify = require('slugify')

const connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'camerascrape'
});

// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// });

var debugMode = false;
var qOffset = 10000;

var trainItemIds = process.argv.slice(2);
if(trainItemIds.length > 0) {
	console.log(trainItemIds);
	trainItemIds.forEach(function(example) {
		trainKnownItem(example);
	});
} else {
	console.error("Please enter item IDs");
}



function sanatizeString(string)
{
	string = string.toLowerCase().split(" for ",1).shift();
	string = string.toLowerCase().split(" with ",1).shift();
	string = string.toLowerCase().split(" and ",1).shift();
	string = string.toLowerCase().split(" w/ ",1).shift();
	string = string.replace('\t','').trim();
	string = string.replace('(','');
	string = string.replace(')','');
	return string;
}

function sanatizeLensString(string)
{
	string = string.replace(/\d[dge]/gi, function(s) { //fixes f/2.0D f/2.8E f/5.6G
		return ( s.substring(0,s.length-1) + " " + s.substring(s.length-1, s.length) );
	});
	string = string.replace(/\d\s+m{2}|\d-\s+\d|\d\s+-\d/gi, function(s){ // fixes: 24 mm, 12 -24mm, 12- 24mm
		return s.replace(' ', '');
	});
	string = string.replace(/[f\/-]\d+\.0\s/g, function(s){ // fixes: f/2.8-4.0, f/4.0 > f/4
		return ( s.substring(0,s.length-3) + " ");
	})
	string = string.replace(/\d+-\d+\s/g, function(s){ // fixes 80-200 > 80-200mm
		return ( s.substring(0,s.length-1) + "mm ");
	})
	//misc fixes
	string = string.replace("series e", "series_e");
	string = string.replace("non ai", "non_ai");
	string = string.replace("pre ai", "pre_ai");
	if(! string.includes("f/")) {
		string = string.replace(/\s\d+\.\d+\s/, function(s){ // fixes 80-200 > 80-200mm
			return ( " f/"+s.substring(1));
		})	
	}
	return string;
}

// function splitTrain(words, category)
// {
// 	words.split(' ').forEach(function(word) {
// 		t1Classifier.learn(word, category);
// 	});
// }

function splitItemIntoKeywords(item, callback)
{
	searchKeywords(item, function(result) {
		console.log(item);
		console.log(result);
	});
}

function searchKeywords(itemName, callback)
{
	itemName = itemName.replace(/\s\s+/g, ' '); // reduce multiple white space in string
	itemName = sanatizeLensString(sanatizeString(itemName)); // remove tabs, trim
	// console.warn("item sanitized name: "+itemName);
	var keywordList = itemName.split(' '); 
	var params = [keywordList];
	var slugParts = [];
	var keywordId = [];
	var highestTier = 0;
	// console.warn(keywordList);

	return connection.query(`
	SELECT id, keyword, tier
	FROM keywords
	WHERE id IN (
	SELECT
	CASE
		WHEN synonym IS NOT NULL THEN synonym
		ELSE id
	END as top_number
	FROM keywords
	WHERE keyword IN (?)
	)
	AND tier < 8 # cut out relevant but fluff keywords
	ORDER BY tier ASC, keyword ASC
	;`, params, function (error, results, fields) {
		if (error) throw error;
		results.forEach(function(entry) { 
			slugParts.push(entry.keyword);
			keywordId.push(entry.id)
			if(highestTier+1 == entry.tier) highestTier = entry.tier;
		});	
		return callback({
			highestTier: highestTier,
			slugParts: slugParts,
			keywordId: keywordId,
			slug: slugify(slugParts.join(" "), {remove: /[-/*+~.()'"!:@]/g,lower:true})
		});
	});
}

function classifyItem(item, callback)
{
	var itemName = item.title;
	searchKeywords(itemName, function(keyResult) {
		// console.log(itemName);
		//  console.log(keyResult);
		// var params = [keyResult.keywordId.length]; //keyResult.keywordId,
		if(!keyResult.keywordId.length){
			console.warn("Classify Failed with no keywords: #" + item.id + " " + itemName);
			return callback([]);
		} else {
			compareToItems(keyResult.keywordId, function(itemSet) {
				if(debugMode) {
					console.warn(keyResult);
				}
				if(!itemSet.length) {
					if(debugMode){
						console.warn(keyResult);
					}
					salvageClassification(item, keyResult, function(newItemSet) {
						if(!newItemSet.length) {
							console.error("Classify failed with keywords, but no items with keyword matches: #" + item.id + " " + itemName);
						} else {
							console.log("Classify salvaged for: "+itemName);
						}
						return callback(newItemSet);
					});
				} else {
					console.log("Classify found "+itemSet.length+" matches for "+itemName);
					if(itemSet.length > 1) {
						// console.warn(itemSet);
					}
					return callback(itemSet); 
				}
			});
		}
	});
}

function salvageClassification(item, failedQueryResult, callback)
{
	// get keywords of tier level-1
	var reducedTier = failedQueryResult.highestTier-1;
	var params = [failedQueryResult.keywordId, reducedTier];
	connection.query(`
		SELECT id, keyword, tier
		FROM keywords
		WHERE id IN (?)
		AND tier < ?
		ORDER BY tier ASC, keyword ASC
		;`, params, function (error, results, fields) {
			if (error) throw error;
			var reducedItemNameParts = [];
			var reducedItemKeywordIds = [];
			results.forEach(function(entry) { 
				reducedItemKeywordIds.push(entry.id);
				reducedItemNameParts.push(entry.keyword);
			});	
			var reducedItemName = reducedItemNameParts.join(" ");
			if(reducedItemKeywordIds.length > 0) {
				compareToItems(reducedItemKeywordIds, function(itemSet) {
					if(debugMode && itemSet.length > 0) {
						getItemsToExplain(itemSet);
					}
					if(itemSet.length > 0) {
						getItemKeywords(itemSet, function(realItemKeywordIdSet){
							var badItemKeywordIdSet = failedQueryResult.keywordId;
							var correctedItemKeywordIdSet = badItemKeywordIdSet.filter(value => -1 !== realItemKeywordIdSet.indexOf(value));
							if(debugMode) {
								console.log(realItemKeywordIdSet);
								console.warn(badItemKeywordIdSet);
								console.warn(correctedItemKeywordIdSet);
							}

							//now mach against that
							compareToItems(correctedItemKeywordIdSet, function(itemSet) {
								if(!itemSet.length) {
									console.error("Classify failed with keywords, but no matches: # " + item.id + " " + item.title);
									if(debugMode){
										console.warn(itemSet);
									}
								} else {
									if(itemSet.length > 1) {
										console.warn("Classify returned multiple matches for #" + item.id);
									} 
								}
								if(debugMode && itemSet.length>0	) {
									console.warn("Fixing bad title: "+item.title+" to match:");
									getItemsToExplain(itemSet);
								}
								return callback(itemSet);
							});
						});
					} else {
						return callback([]);
					}
				});
			} else {
				return callback([]);
			}

			console.warn("Reduced #" + item.id + ": " + reducedItemName);
			// next step: get all matched of above, eliminate sub-keywords agasint this one's


			// return callback({
			// 	highestTier: highestTier,
			// 	slugParts: slugParts,
			// 	keywordId: keywordId,
			// 	slug: slugify(slugParts.join(" "), {remove: /[-/*+~.()'"!:@]/g,lower:true})
			// });
		});
}

function getItemsToClassify(callback)
{
	connection.query(`
	SELECT id, title
	FROM feedposts
	WHERE feed_id = 2
	AND title LIKE "%nikon%"
	AND (
			title NOT LIKE "%box only%"
		OR 	title NOT LIKE "%empty box%"
	)
	AND processed IS NULL
	ORDER BY id DESC
	LIMIT 20000
	;`, function (error, results, fields) {
		if (error) throw error;
		console.log("Found "+results.length+" feedposts to work");
		results.forEach(function(item){
			classifyItem(item, function(itemMatches) {
				if(itemMatches.length == 1) {
					console.log("Write: "+item.title+" successful with 1 match");
					writeFeedpostItemMatch(item.id, itemMatches[0]);
					
				}
				markItemProcessed(item.id, itemMatches.length);
			});
			// process.exit();
			// if(typeof itemMatches != undefined && itemMatches.length == 1) {
			// 	writeFeedpostItemMatch(item.id, itemMatches[0].id);
			// 	console.log("wrote " + item.name);
			// }
		});
		writeAvgPrice();
		// return callback(true);

	});
}

function getFeedpostsToExplain(feedposts) 
{
	var params = [feedposts];
	connection.query(`
		SELECT id, title
		FROM feedposts
		WHERE id IN (?)
		;`, params, function (error, results, fields) {
		if (error) throw error;
		console.log("Found "+results.length+" feedposts to work");
		results.forEach(function(item){
			classifyItem(item, function(itemMatches) {
				console.log(item.id+" : "+item.title);
				console.warn(itemMatches);
				if(itemMatches.length>0) {
					getItemsToExplain(itemMatches);
				}
			});
		});
	});
}

function getItemsToExplain(items)
{
	var params = [items];
	connection.query(`
		SELECT id, name
		FROM items
		WHERE id IN (?)
		;`, params, function (error, results, fields) {
		if (error) throw error;
		var itemNames = [];
		results.forEach(function(item){
			itemNames.push(item.name);
		});

		console.log("Found "+results.length+" items to match for salvage");
		console.log(items);
		console.log(itemNames);
	});
}

function getItemKeywords(items, callback)
{
	//get all the keywords from a set of items
	var params = [items];
	connection.query(`
		SELECT DISTINCT keyword_id
		FROM items_keywords
		WHERE item_id IN (?)
		;`, params, function (error, results, fields) {
		if (error) throw error;
		var keywordIds = [];
		results.forEach(function(row){
			keywordIds.push(row.keyword_id);
		});
		return callback(keywordIds);
	});
}

function markItemProcessed(itemId, statusId)
{
	// null/0 = unprocessed
	// >1 = number of matches
	// 1 = pass
	var params = [statusId, itemId];

	connection.query(`
		UPDATE feedposts
		SET processed=?
		WHERE id=?
		`, params, function (error, results, fields) {
		if (error) throw error;
	});	
}

function writeFeedpostItemMatch(feedpostId, itemId)
{
	var params = [feedpostId, itemId, 1]; //keyResult.keywordId,
	connection.query(`
	INSERT IGNORE INTO feedposts_items (feedpost_id, item_id, visible)
	VALUES (?, ?, ?)
	;`, params, function (error, results, fields) {
		if (error) throw error;
	});	
}

function compareToItems(idSet, callback)
{
	var params = [idSet, idSet.length]; //keyResult.keywordId,
	return connection.query(`
	SELECT items.*
	FROM items, items_keywords
	WHERE items.id = items_keywords.item_id
	AND items_keywords.keyword_id IN (?)
	GROUP BY items.id
	HAVING count(*) = ?
	;`, params, function (error, results, fields) {
		if (error) throw error;
		foundItemIds = [];
		results.forEach(function(item){
			// console.log("Search found #"+item.id+" "+item.name);
			foundItemIds.push(item.id); //+" "+item.name);
		});
		
		return callback(foundItemIds);
	});	
}




function trainKnownItem(itemId)
{
	//EX: Nikon AF-S Nikkor 300mm f/2.8G VR IF-ED	
	var params = [itemId];
	connection.query(`
	SELECT id, concat(brand, " ",name) as item_name
	FROM items
	WHERE id = ?
	;`, params, function (error, results, fields) {
		if (error) throw error;
		results.forEach(function(entry) { 
			searchKeywords(entry.item_name, function(keyResult) {
				if(keyResult.highestTier >= 3) {
					console.log("writing T"+ keyResult.highestTier+" item " + entry.item_name);
					writeItemKeywords(itemId, keyResult.keywordId);
				} else {
					console.warn("Item max tier is insufficient: T" + keyResult.highestTier + " for " + entry.item_name);
					console.error(keyResult);
					// process.exit();
				}
			});
		});	
	});
}

function writeItemKeywords(itemId, keywordIds)
{
	// console.log(Array.isArray(keywordIds));
	// process.exit();
	// console.log("writing item "+ itemId);
	var pairs = [];
	keywordIds.forEach(function(keywordId) {
		pairs.push([itemId, keywordId]);
	});
	var params = [pairs];
	connection.query(`
	DELETE FROM
	items_keywords
	WHERE item_id = ?
	;`, itemId, function (error, results, fields) {
		if (error) throw error;
		connection.query(`
		INSERT INTO items_keywords
		(item_id, keyword_id)
		VALUES ?
		;`, params, function (error, results, fields) {
			if (error) throw error;
		});
	});
}

/*
// Semi-guided, cascading classification tree
Training
1. load tier 1 examples per tier item
2. confirm classification until P is sufficiently high
3. load next tier..

Usage
1. classify record on tier cascade
2. drop n-1 tiers(s) when P is insufficient or tiers have been exhausted

Ex results
1. nikon-50mm
2. nikon-50mm-f1.8-afd : also positive for users searching for #1
3. nikon-50mm-f1.8-g : also positive for #1, negative for #2
*/
