const mysql      = require('mysql');
const console = require('better-console');
const connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'camerascrape'
});
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// });

var qOffset = 0;
var itemsToClassify = [];
getItems();



function getItems()
{
	var limit = 5000;
	console.warn("running "+qOffset+" : lim: "+limit);
	connection.query(`
	SELECT id, title
	FROM feedposts
	ORDER BY id ASC
	LIMIT ? OFFSET ?
	;`, [limit, qOffset], function (error, results, fields) {
		if (error) throw error;
		console.warn("Found "+results.length+" feedposts to work");
		results.forEach(function(item){
			itemsToClassify.push(item);
			// writeGenericWords(item);
		});	
		qOffset += limit;
		if(results.length > 0){
			workItemList();
		} 
	});
}

workItemList = function()
{
	if(itemsToClassify.length == 0){
		getItems();
	} else {
		if(itemsToClassify.length % 100 ===0){
			console.log("working list size of "+itemsToClassify.length);
		}
		writeGenericWords(itemsToClassify.pop(), workItemList);
	}
}

function writeGenericWords(item, callback)
{
	// console.log(item.title);
	item.title = item.title.replace(/\s\s+/g, ' ').toLowerCase().replace('\t','').trim();
	var pairs = [];
	item.title.split(' ').forEach(function(word) {
		pairs.push([word, item.id]);
	});
	var params = [pairs];
	
	connection.query(`
		INSERT IGNORE INTO post_words
		(word, post_id)
		VALUES ?
		;`, params, function (error, results, fields) {
			if (error) throw error;
			callback();
		});

}

////


function sanatizeString(string)
{
	string = string.toLowerCase().split(" for ",1).shift();
	string = string.toLowerCase().split(" with ",1).shift();
	string = string.toLowerCase().split(" and ",1).shift();
	string = string.toLowerCase().split(" w/ ",1).shift();
	string = string.replace('\t','').trim();
	return string;
}

function sanatizeLensString(string)
{
	string = string.replace(/\d[dge]/gi, function(s) { //fixes f/2.0D f/2.8E f/5.6G
		return ( s.substring(0,s.length-1) + " " + s.substring(s.length-1, s.length) );
	});
	string = string.replace(/\d\s+m{2}|\d-\s+\d|\d\s+-\d/gi, function(s){ // fixes: 24 mm, 12 -24mm, 12- 24mm
		return s.replace(' ', '');
	});
	string = string.replace(/[f\/-]\d+\.0\s/g, function(s){ // fixes: f/2.8-4.0, f/4.0 > f/4
		return ( s.substring(0,s.length-3) + " ");
	})
	string = string.replace(/\d+-\d+\s/g, function(s){ // fixes 80-200 > 80-200mm
		return ( s.substring(0,s.length-1) + "mm ");
	})
	//misc fixes
	string = string.replace("series e", "series_e");
	string = string.replace("non ai", "non_ai");
	string = string.replace("pre ai", "pre_ai");
	if(! string.includes("f/")) {
		string = string.replace(/\s\d+\.\d+\s/, function(s){ // fixes 80-200 > 80-200mm
			return ( " f/"+s.substring(1));
		})	
	}
	return string;
}

// function splitTrain(words, category)
// {
// 	words.split(' ').forEach(function(word) {
// 		t1Classifier.learn(word, category);
// 	});
// }

function splitItemIntoKeywords(item, callback)
{
	searchKeywords(item, function(result) {
		console.log(item);
		console.log(result);
	});
}

function searchKeywords(itemName, callback)
{
	itemName = itemName.replace(/\s\s+/g, ' '); // reduce multiple white space in string
	itemName = sanatizeLensString(sanatizeString(itemName)); // remove tabs, trim
	// console.warn("item sanitized name: "+itemName);
	var keywordList = itemName.split(' '); 
	var params = [keywordList];
	var slugParts = [];
	var keywordId = [];
	var highestTier = 0;
	// console.warn(keywordList);

	return connection.query(`
	SELECT id, keyword, tier
	FROM keywords
	WHERE id IN (
	SELECT
	CASE
		WHEN synonym IS NOT NULL THEN synonym
		ELSE id
	END as top_number
	FROM keywords
	WHERE keyword IN (?)
	)
	AND tier < 8 # cut out relevant but fluff keywords
	ORDER BY tier ASC, keyword ASC
	;`, params, function (error, results, fields) {
		if (error) throw error;
		results.forEach(function(entry) { 
			slugParts.push(entry.keyword);
			keywordId.push(entry.id)
			if(highestTier+1 == entry.tier) highestTier = entry.tier;
		});	
		return callback({
			highestTier: highestTier,
			slugParts: slugParts,
			keywordId: keywordId,
			slug: slugify(slugParts.join(" "), {remove: /[-/*+~.()'"!:@]/g,lower:true})
		});
	});
}

function classifyItem(item, callback)
{
	var itemName = item.title;
	writeGenericWords(item);
	searchKeywords(itemName, function(keyResult) {
		// console.log(itemName);
		//  console.log(keyResult);
		// var params = [keyResult.keywordId.length]; //keyResult.keywordId,
		if(!keyResult.keywordId.length){
			console.warn("Classify Failed with no keywords: "+itemName);
			return[];
		}
		compareToItems(keyResult.keywordId, function(itemSet) {
			if(!itemSet.length) {
				console.error("Classify failed: "+itemName);
			} else {
				// console.log("Classify found "+itemSet.length+" matches for "+itemName);
				if(itemSet.length > 1) {
					// console.warn(itemSet);
				} 
			}
			return callback(itemSet);
			// process.exit();
		});
	});
}


function cycleWords(goodtogo)
{
	processRawWords();
}





function getItemsToClassify(callback)
{
	connection.query(`
	SELECT id, title
	FROM feedposts
	# WHERE feed_id = 2
	# AND title NOT LIKE "%nikon%"
	# AND (processed < 1
	# OR processed IS NULL)
	LIMIT 5000 #OFFSET 130000
	;`, function (error, results, fields) {
		if (error) throw error;
		console.log("Found "+results.length+" feedposts to work");
		results.forEach(function(item){
			classifyItem(item, function(itemMatches) {
				if(itemMatches.length == 1) {
					console.log("Write: "+item.title+" successful with 1 match");
					// writeFeedpostItemMatch(item.id, itemMatches[0]);
					
				}
				// markItemProcessed(item.id, itemMatches.length);
			});

			// process.exit();
			
			// if(typeof itemMatches != undefined && itemMatches.length == 1) {
			// 	writeFeedpostItemMatch(item.id, itemMatches[0].id);
			// 	console.log("wrote " + item.name);
			// }
		});
		// return callback(true);

	});
}

function markItemProcessed(itemId, statusId)
{
	// null/0 = unprocessed
	// >1 = number of matches
	// 1 = pass
	var params = [statusId, itemId];

	connection.query(`
		UPDATE feedposts
		SET processed=?
		WHERE id=?
		`, params, function (error, results, fields) {
		if (error) throw error;
	});	
}

function writeFeedpostItemMatch(feedpostId, itemId)
{
	var params = [feedpostId, itemId]; //keyResult.keywordId,
	connection.query(`
	INSERT INTO feedposts_items (feedpost_id, item_id)
	VALUES (?, ?)
	;`, params, function (error, results, fields) {
		if (error) throw error;
	});	
}

function compareToItems(idSet, callback)
{
	var params = [idSet, idSet.length]; //keyResult.keywordId,
	return connection.query(`
	SELECT items.*
	FROM items, items_keywords
	WHERE items.id = items_keywords.item_id
	AND items_keywords.keyword_id IN (?)
	GROUP BY items.id
	HAVING count(*) = ?
	;`, params, function (error, results, fields) {
		if (error) throw error;
		foundItemIds = [];
		results.forEach(function(item){
			// console.log("Search found #"+item.id+" "+item.name);
			foundItemIds.push(item.id); //+" "+item.name);
		});
		
		return callback(foundItemIds);
	});	
}

function trainKnownItem(itemId)
{
	//EX: Nikon AF-S Nikkor 300mm f/2.8G VR IF-ED	
	var params = [itemId];
	connection.query(`
	SELECT id, concat(brand, " ",name) as item_name
	FROM items
	WHERE id = ?
	;`, params, function (error, results, fields) {
		if (error) throw error;
		results.forEach(function(entry) { 
			searchKeywords(entry.item_name, function(keyResult) {
				if(keyResult.highestTier >= 3) {
					console.log("writing T"+ keyResult.highestTier+" item " + entry.item_name);
					writeItemKeywords(itemId, keyResult.keywordId);
				} else {
					console.warn("Item max tier is insufficient: T" + keyResult.highestTier + " for " + entry.item_name);
					console.error(keyResult);
					// process.exit();
				}
			});
		});	
	});
}

function writeItemKeywords(itemId, keywordIds)
{
	// console.log(Array.isArray(keywordIds));
	// process.exit();
	// console.log("writing item "+ itemId);
	var pairs = [];
	keywordIds.forEach(function(keywordId) {
		pairs.push([itemId, keywordId]);
	});
	var params = [pairs];
	connection.query(`
	DELETE FROM
	items_keywords
	WHERE item_id = ?
	;`, itemId, function (error, results, fields) {
		if (error) throw error;
		connection.query(`
		INSERT INTO items_keywords
		(item_id, keyword_id)
		VALUES ?
		;`, params, function (error, results, fields) {
			if (error) throw error;
		});
	});
}

